#include <iostream>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>

void checkArguments(int arguments){
    if(arguments != 1){
        std::cerr << "Please provide only port as argument!" << std::endl;
        exit(1);
    }
}

void setServer(struct sockaddr_in *server, char *arg){
    std::memset(*server, 0, sizeof *server);
    *server->sin_family = AF_INET;

    int portNumber = std::atoi(arg);
    *server->sin_port = htons(portNumber);
}

int main(int argc, char *argv[]){
    int sockfd, newsockfd, clientLength, returnValue; // fd: file descriptors
    std::string buffer;
    struct sockaddr_in server_address, client_addres;

    checkArguments(argc);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        std::cerr << "Error opening socket" << std::endl;

    setServer(&server_address, argv[1]);
    if (std::bind(sockfd, (struct sockaddr *) &server_address), sizeof server_address)
        std::cerr << "Error on binding socket to server" << std::endl;

    listen(sockfd, 5);

}
